
This diary file is written by Elvira Khamenok N16088432 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* In this week I first had to understand the instructions for this online course. 
* It was a bit confusing because I did not really know how to start. It got better after I could distinguish between the diary and the other assignment.
* I am still not sure whether I am doing the right thing.
* I read through the presentations about success stories of AI and Deep Learning from other students.
* First I was concerned whether I could find a different success story of AI which is interesting. I did not know there were so many.
* I tried to find a success story related to a current topic. I searched for papers about COVID-19 and I found one.
* While searching for success stories I came across many different fields of application of AI. For example Disney is also doing AI-research.


# 2020-03-27 #

* I have read the very comprehensibely written artictle "How should we define AI?"
* Three applications with their implications were highlighted: Self-driving cars, Content recommendation, Image and video Procesing
* Some Implications of AI are beneficial (road safety) and some are challanging or even dangerous (fake videos of events)
* Using "an AI method" does not mean that "an AI" is outperforming a human in every aspect and field (narrow AI)
* Defining AI by it's characteristic properties: Autonomy and Adaptivity
* Watchout: "Suitcase Words" have many different meanings, but sometimes only one meaning is intended when using a suitcase word
* AI is not really countable, it is a discipline like mathematics or biology 
* Deep Learning (complex mathematical models for learning) is a subfield of Machine Learning (improving performance with more experience) is a subfield of AI is a subfied of computer sience
* Data Science covers many subdisciplines like Machine Learning, but is not necessarily including AI methods
* A robot has sensors and actuators, the shape is usually disigned according to the application (mainly non-humanoid)
* AI in the practical world is not really "intelligent" but it acts intelligent in a certain situation/task
* Speaking about real and at this point applicable AI usually refers to "narrow AI" as opposed to "general AI"
* I have learnt very much about AI in general
* I think discussing about philosophy regarding AI is very interesting although in reality it is not that important at least for now
* It is actually interesting how thinking about AI and what it is lets people also think about humanity and question what it takes to be intelligent and have a mind, things we usually consider as naturally given

# 2020-04-05 #

* This week I started learning Python
* There are many resources on the internet
* I decided to go along with a YouTube Video of over 6 hours
* Python has a simple and clear syntax
* Can be used for many purposes like Automation and AI

# 2020-04-12 #

* Went through NordlingLab-Tutorial for Python
* Good summary but for a Python/Programming-Beginner not that easy to understand
* Watched more of the Python YouTube-Video for further understanding
* Misunderstanding of organisation: I finished the work for week 4, but we seem to be in week 6
* For the next two weeks, each week I will learn the material of two weeks

# 2020-04-19 #

* Attended the online lecture, understood the organization better
* Watched the Video: What is a neural network?
* Watched the Video: How machines learn
* Keywords: neurons, layers, activations and weights, weighted sum and bias, Sigmoid/logistic curve
* Learning: Finding right weights and biases
* Read Chapter 5.1 of Deep Learning Book
* Need to take more time to understand this better, more complicated than expected

# 2020-04-26 #

* Read Chapters 5.1 to 5.3 of Deep Learning Book
* Keywords 5.1: Task, Measure of performance, Experience, Classification, Regression..., training data set, test data set, supervised/unsupervised learning
* Bias: output is biased towards a certain value without any input
* Keywords 5.2: training error, test/generalization error, data generating process (i.i.d. assumptions)
* Underfitting: model already performs bad on the training set
* Overfitting: model performs very good on training set, but bad on test set
* Controlling Over/Underfitting by altering the capacity of the model
* No free lunch theorem, Regularization (read again later)
* Keywords 5.3: hyperparameters, validation data set as subset of the training set used for update of hyperparameters during/after training
* Watched introduction to TensorFlow by M.Görner
* Keywords: softmax, cross-entropy loss, one-hot encoding, sigmoid, relu
* Tried to train the basic model by running M.Görner's Colab, unfortunately there was an error which I could not solve for now
* Error Code in "Imports": "AttributeError: module 'tensorflow' has no attribute 'enable_eager_execution'"

# 2020-05-03 #

* Watched "How Convolutional Neural Networks work" by Brendon Rohrer
* Read Chapters 9.1 to 9.3 of Deep Learning Book
* Reading the book I understood the concepts from the video better
* Description of convolution using time-series data as an example is quite hard to understand, picture as an example is better
* The filter which the video refers to equals the kernel which the book refers to?
* One Convolutional Layer: 1. Convolution stage, 2. Detector stage (ReLU), 3. Pooling layer (max pooling)
* Motivation for Convolution: sparse interactions, parameter sharing, equivariant representations
* Sparse interactions: kernel smaller than input
* Parameter sharing: I need a good example for that, did not fully understand this concept
* Equivariance: Input changes, so output changes in the same way
* Pooling helps to make the output invariant to small transitions of the input
* ReLU(Rectified Linear Unit): negativ values will be changed to zero, positiv values remain the same
* I have to catch up with topic 8 and 9 before group project starts, I had to invest more time in another subject first

# 2020-05-10 #

* Watched videos about gradient descent and backpropagation
* Read material of week 8 partly
* Following week I plan to finish week 8 and 9 and to start with the group project
* I decided to work alone on the group project
* You learn much in a group, but in my home university I have this opportunity very often and it is mandatory
* As an exchange student I am lucky to meet many people everyday, but for this one project I would like to finish it myself and to be able to work straightforwardly

# 2020-05-17 #

* Finished material of topic 8
* I really liked reading the chapter by Michael Nielsen, helps to understand previous material better
* Watched the videos about ethics and algorithm bias
* Attended discussion in class
* I especially liked the videos of Rob Miles 
* The discussion in class was a bit short, I hoped we would have discussed the last two videos more
* But I found out that many students seem to like the videos of Rob Miles, so we talked a bit after class
* Following week I will look into topic 9, but my priority will be working on the group project

# 2020-05-24 #

* Started preparing the group project
* Refreshed knowledge of Python basics and watched some tensorflow tutorials
* Searched for an article for the Article Summary, maybe I can apply some of the ideas, which I found while searching, on my own DNN
* Wrote the article summary
* Watched Martin Görners TensorFlow and Deep Learning without a PhD again, I have the feeling that I need to get more used to the basics
* I am not sure what I should do about topic 9, since there is no material, how deep should I go on this topic?

# 2020-05-31 #

* Attended the lecture about automation
* Really enjoyed this lecture and the discussion
* I do not have a specific dream-job. The world is changing so fast and I want to be open and see what opportunities there will be. Probably I will have a job which does not exist today.
* Some of the points in the lecture seemed a bit too simple. Although I also believe that AI can help humanity with many current problems, I also think that not all the changes can be done as easily.
* I wander what political impact it would have if the world would suddenly switch to renewable energy only
* Worked on the presentation of the article
* Due to another homework-project I could not work on the AI-project this week

# 2020-06-07 #

* Finished the presentation for Article Summary
* Attended the second part of automation lecture
* Met my taiwanese buddy from foreign language department who is recently taking programming courses. I asked her why actually? She told me that as a linguist you have much better job chances when specialising on AI, e.g. for natural language processing, translation etc.
* We had an interesting talk, although linguistic seems to be very different to mechanical engineering, AI is kind of connecting different disciplines now
* Worked on my project

# 2020-06-15 #

* I mainly worked on my project
* Using Convolution and Dense layers I reached a plateau of accuracy which I could not surpass first
* Then I found out how to change parameters of the optimizer which I am using and I managed to get higher accuracy by using a learning rate schedule for learning rate decay
* For the presentation of the Article Summary I prerecorded an YouTube video
* Attended the class and listened to the presentations of other Article Summaries

# Article Summary #

* Training Deep Neural Networks for object detection using supervised learning usually focuses on the algorithms within the DNN often using real-world data sets for training, validation and testing. At the first glance real-world data seems to be preferred for training, but at the same time it is very cost and labor intensive to obtain real-world data and to label it correctly.
* But is the result of training a DNN with real-world data really better than training with synthetic data? Although there is already some work on generating synthetic data sets for training, most of them concentrate on generating real-world looking images. Which is in another way also quite expensive and labor intensive.
* Tremblay et al. present a way to generate synthetic data using domain randomization (DR) of specific parameters which is inexpensive and able to create a huge amount of data very fast. Using DR on specific parameters means that you create an image which contains the object, which is to be recognized, and randomly vary parameters, e.g. number or texture of the object or shapes of flying distractors. This way you obtain non photorealistic images, but it also forces the network to focus on the relevant objects.
* DNNs trained with DR data sets deliver competitive results evaluated on real-world data sets. 
* This approach to training a DNN using domain randomized synthetic data and the results achieved this way can help to understand better how deep learning works. It shows that a DNN is not really learning to recognize a specific object like a human would do in an intuitive way, so it is not essential to use real-world images to train recognizing a specific object. Due to abstraction it might be more useful to just present the object in a way with as much as possible variety. In the end the DNN does not really care about the background, at least not if it is not a specified task.

* (My question for discussion: Theoretically, would it be possible to create only one large image containing enough abstract information to train a DNN to recognize a specific object? Would such an image probably look like white noise to a human?)
 