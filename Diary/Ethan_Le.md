This diary file is written by Ethan Le in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

* This is the diary for the first two lectures of the course, so far so good I think. 
* Seems like that I got a lot of thing to learn from simple stuff like version control via Git or Python programming to AI, DL, ML...
* Should take a look at the Markdown syntax stuff, looks similar like Latex somehow. 
* Although sounds kinda fancy (the AI stuff), the model you got is only as good as your data. 
* Should revise a little bit about basic statistic. 
* Have good feeling, feel excited to learn one thing or two. 