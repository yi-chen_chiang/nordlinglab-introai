This diary file is written by Francisco Roman F44077027 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #
# Lecture 1 #
* The homework of the first week seemed like a simple task, but when doing it I realized the number of successes achieved by artificial intelligence,
* its growth is definitely fast, effective, accurate and very necessary for the future.
* I had a little trouble understanding the website, but I feel it will be a very useful and necessary tool for the future, especially for the workplace.
* I have high expectations for this class, I hope to learn a lot.

# 2020-03-22 #
# Lecture 2 #
# Summary of the article #
# How should we define A.I.? #
* We have not been able to develop an artificial intelligence that can cover all areas, 
* or that can solve anything, rather they are focused on something specific or for something specific.
* Artificial intelligence is better defined as a discipline.
* are generally designed to focus on something specific
# Fields relate to A.I. #
* The handling of large amounts of data, ex: statistics, economy, etc.
* robotics and everything related to it.
# Philosophy of AI #
* Based more on steps to follow than to prove to be a thinking entity.
* can pose as a person without problem, at least for a few minutes (simulating an online conversation for example)
* We have not been able to develop an artificial intelligence that can cover all areas, 
* or that can solve anything, rather they are focused on something specific or for something specific.
* Artificial intelligence is better defined as a discipline.
* are generally designed to focus on something specific
# 2020-03-29 #
* We did not have homework this week
* I hope the next class could cover python topics, or at least an introduction to it
# 2020-04-05 #
* We did not have homework or any assignments this week
* I will be waiting for some python related content, and spend some times watching or reading about it 
# 2020-04-12 #
* Attend online class
# 2020-04-19 #
* no hw for this week.
* getting ready for second class online, reviewing concepts.
# 2020-04-26 #
* we started seing the first code lines for python
* really interesting
# 2020-05-03 #
* I have not watched the video yet, I will do it tomorrow before this week's class.
* Looking forward for the next class 
# 2020-05-10 #
* We had a Q&A class relating codes and the proposed exercises for python
* also talked about the final project for the course
# 2020-05-17 #
* we were designated with our respective groups
* starting from next week we will attend normal classes, given that the cases of covid-19 have been reduced
* I am looking forward to attend the class and discuss the correspondent topic 
# 2020-05-24 #
* Today we had our first discussion session 
* I found interesting how everyone had a different opinion and also shared their experiences refering to AI
* I agree with the final conclusion that to be able to coexist with a AGI we first need to overcome some 
* bad human's behaviors and learn how to treat each other more fairly, and thats possibly the only way 
* in which the AGI and human can establish a safer relationship.
* I am looking forward for the next class discussion.
# 2020-05-31 #
* We had our first discussion this week 
* we divided the parts for the final project and started working on it 
* we will have another meeting during this week 
# 2020-06-07 #
* We had the second discussion this week about automation 
* I found the class really interesting because we discussed the differents advantages and disadvantages of automation
* We also discussed the impacts of automation in the last decade and in the near future ( around 20 years from now)
* We also visualized our futures jobs and how automation affect them 
* My group and I almost finish the final project for this course, we are looking forward to improve the accuracy of our AI before doing the video
* We have one more meeting next tuesday to talk about the video 
# 2020-06-14 #
* Finally, this week we managed to finish our term project video editing
* Each one of us explained a part of the code and we recorded it through a cellphone
* the quality of the audio was edited in the software later for one of our members
* the video looks really well and we are excited for the last discussion this week
# 2020-06-21 #
* This week is our final presentation and our final exam
* and also our last class for this semester
* I will be reviewing all the concepts and code parts for the final exam
* and I am also excited to see others people final project
# 2020-06-24 #
* Today was the final exam, But the time was not enough for it
* I would like that the professor gives more time next year, for the final exam
* The few projects that we saw were all really interesting and I think all of them did a really good job
