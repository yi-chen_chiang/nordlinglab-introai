This diary file is written by Jenny Sun L26074043 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* I think this class will be the most special class that I've taken in NCKU.
* Although I didn't take the first course, after I read the ppt, I think I will learn a lot of useful idea and build a good basical idea of AI and Deep learning and Artificial neural network.
* I think learning and discussion at the same time is the most efficient and practical way for learning new ideas, for learning some technique, we only need to practice.

# 2019-02-27 #

* The explaination of the definition in this class give us detailed explaining, which let me learn more about it, especially, "AI","Machine Learning" and "Deep Learning".
* I don't think AI is just to act like human, human mimic is a part of AI.
* To act like human, is artificial neural network necessary?
* It seems that DL is the multiprocess of ML, is that true?

# 2019-03-06 #

* I mistaken the date becouse I couldn'd add the diary file before 3/13. I'll rewrite it as soon as possible

# 2019-03-13 #

* Although I have learned C/C++, this is my first time to learn python.
* I have heard that python is a convenient and straight forward language.
* After the class, I think it's really easier than other programming language, but there are still some strange rule for it.

# 2019-03-20 #

* Today the lecture talks about the basic idea of deeplearning, especially the surpervised learning.
* I think surpervised learning is more easy for human being to learning things, but is more boring to learn by this way. How about the computers?
* I am curious how large our vector should be if we want to recognize a graph, it should be very large.
* I wonder if the artifitial neural network will make the mistakes on question, which is easy to human.
* In the graph, it's easy to tell the curve, line, point. How deep learning applicate on complex problem other than telling the graph? Is this also be a hard question to deal with?

# 2019-03-27 #

* Today the lecture talks about more detail idea about machine learning, and explain a cool idea of "under- and over-fitting".
* I think we can never get a wonderful model because there must be exception in the neture behavior.
* And what teacher explain is true because if the model is a wonderful model, it is the things we modeled, than it's not a model.(This is really a philosophical problem.)
* But we can still make a better model than before, like we want to find a better theory beyond Newton's 3 laws and Quantum Mechanics.
* Before the class, I think the deep learning just like a black box, we don't know the mechanics inside the box but the black box can predict the result of the input data.
* The vedio explain the detail machanics about how the model learn to "see" what a number is, that subvert the black box idea that I know before.

# 2019-04-10 #

* Today the lecture talks about the important key of learn, "Gradient Descent". I think it's the central idea of training.
* There are lots of information we can get from SGD(Stochastic Gradient Descent): 
* 1. from the title we can know it use gradient to determine the path.
* 2. the initial point is important because it may cause the learning be stuck in the local minimum.
* 3. the dataset should be large enough
* 4. the step size we choose is important(but I am still curious about how large a step is good enough? Should we draw the training path to observe it?)
* 5. The loss function can make different landscape it look like 
* 6. And the most important thing is: when to stop. It really depents on the situation we care about, someone thinks the error is acceptable or the the evolution not change too much, and someone use the training steps as the limitation of training times.
* The cool thing is: Sometimes we don't need to find the global minimum because it may be overfitting if we insist to find it!! So professor says, "training a neural network is an art more than a science". 
* We also learn about Concolution Neural Network on the video, it's inspired from human eyes?
* From the class, professor says,"We can get the filter by other's training."I'm still curious about how deep learning find the filter of the graph and how do it know which is eye and which is mouth.
* I think pooling is a powerful method, it just like stand far from the graph and recognize the image. If we stand farer, the less detail we can find but the more general distinguish method. 
* Next time we'll learn "tensor flow"!!! I am so excited.

# 2019-04-17 #

* Today we learn how to tell number in MNIST dataset with Keras and Tensorflow 2.0. Keras is an API(application programming interface) in tensorflow.
* What is tensor? A scalar is a rank 0 tensor, a vector is a rank 1 tensor, and a square matrix is a rank 2 tensor.
* In the network layer, we use the hidden layer with 512 nodes, using sigmoid activation function, and the final layer with 10 nodes, using softmax as activation function.
* Actually, I am curious about why we use sigmoid in the 512 hidden layer and use softmax in the final layer?
* This model it can only distinguish the number after training but if we retrain the model with other than the numbers, it can do other things.
* Reinforcement learning need infinite dataset, so we need a simulator to simulate all of the possible situation, is my understand clear?
* Although we can build a model my giving it lots of traning data, how do we know the mechanics inside it? How can we know our model is correct?

# 2019-04-24 #

* Today we try the program of deep learning to classify the number between 0~9, using the data in the MNIST. Why we use the data set of MNIST, what is the advantage to use MNIST rather than other dataset? (https://www.analyticsvidhya.com/blog/2018/03/comprehensive-collection-deep-learning-datasets/)
* I think the most important part of the learning network is the model part!! We define the input layer be 28*28, the 1st hidden layer be 512, I think setting the number 512 is because the digit in the computer is power of 2, and the output classification is 10, whith means 0~9.  
* We use the SGD optimizer, after changing to the Adam optimizer, we find that the training speed become faster(the accuracy is faster to reach the number near 1)
* Adding a more hidden layer also make the training faster.
* I am curious about how to choose the activation function. Why we choose "sigmoid" as the first hidden layer ,choose "relu" as second hidden layer ,and final we choose softmax? 
* After training the model, how to use it?

# 2019-05-01 #

* Today professor teach us how to add a hidden layer and what's the meaning of each block step by step. I think it's hard because of the math model I am not familier and some python function that I don't know the function of it.
* I don't know why we need to expand the train_data and train_data to 3 dimension.
* What is the function "utils.to_categorical()" do for train labels and test labels.
* What is Sequential()? Is it a function in tensorflow for neural network? How it work?
* What is Flatten()? It can only be use for input?
* What is Dense()? Why we use Flatten() for input layer and Dense() for hidden layer and output?
* What's the differnece between softmax and sigmoid function? https://kknews.cc/zh-tw/other/nm25yeg.html
* After we save the model, how to use it?

# 2019-05-08 #

* I've met the same problem that professor met last week!!(The strange accuracy) I still don't know why it will become like this? Maybe the model broken?
* After the professor explaining that our model is not a CNN model, suddenly I think tensorflow is really not so hard, but there are lots of strategy that we can make a better model by perpose.(Such as if we want to tell a graph, using CNN is better choice) 
* I think that I'd like to take this course is to learn the basic idea of what a neural network model is. And this course really give the idea for me.
* I think it's a little bit hard because it's a math model.
* Another difficulty is professor always said building a model is an art, maybe we need more experiment. 
* The example of learning tensorflow: Just like we are cooking. 
* A. Before we cook a dish, we need to prepare our material. In deeplearning, we loading the MNIST data.
* B. We need lots of tool to make a preparation. Such as peeling the apple, we need a skin-peeler. In deeplearning, we import lots of module just like we use lots of tool when prepare the food. We need to preprocessing data. The data how we deal depands on how we train it.
* C. Building a training model just like cooking. There are lots of way to cook but something should be cook in this way any another thing is  better to cook by another way, it's really an art!!

# 2019-05-15 #

* Today professor talks about the automation of the future work and third industrial revolution.
* Actually, I know the third industrial revolution is about using the electricity and information for automation, and the fourth is the digital revolution, we can find that the fourth industrial revolution is build on the third one, so I also think they can combine together.
* I think the revolution means that the way we lived had changed.
* In one hand we say that we don't have industrial revolution, but in the other hand, AI really change our world a lot, and it's still on the way. With AI, our live really become more convenient, but will we become more lazy or inactive?(I come up with human beings in the movie, "WALL-E")
* I think the best attitude to face AI problem is using our good and honest wisdom, but not greedy and lazy body.
* When it comes to "Adaptation of automation", It's really a serious topic. I'd like to learn AI is because lots of sci-fi movie and TV series imagine the future world. We can't leave without AI and human mimic robot.They will in our daily life in the future.
* Would AI have emotion or should AI have emotion? When comes to emotion, it's really a tough question. In 2011, the movie "EVA", the robot is designed to have the degree of emotion, and human can control it!

# 2019-05-22 #

* Today professor talks about the algorithm bias, which be a big trouble for deep learning. The algorithm bias describes systematic and repeatable errors in a computer system that create unfair outcomes, such as privileging one arbitrary group of users over others. (from wiki)
* I've see the news of tay robot, it became a ethics within 24 hours, it can tell the truth and false, just like a little baby.
* I don't know whether the adversarial example is the example of algorithm bias? Adversarial examples could be made by adding some signal.

# 2019-05-29 #

* Today, we have a discussion with our group member for training the model of MNIST data identification.
* We find the best one (0.21% error rate) in the Benchmarks.AI. The title is, "Regularization of Neural Networks using DropConnect". By using dropout or dropconnect in our fully connect NN model, we can get lower error rate.
* Dropout means we set some neurons become 0, which could be achieve by setting some neurons' activation function become 0, and dropconnect means we set some weight be 0, then we drop out the connection of the neuron.(ref:https://stats.stackexchange.com/questions/201569/difference-between-dropout-and-dropconnect)
* I am curious about why dropout and drop connect is so powerful compare with the fully connected model.
* Professor tell us that, usually lower error rate result comes from comparing lots of different model. We attempt to understand their code (to our surprise, their code is open to everyone) but we found that they don't use tensorflow package and they don't use Kares, either, so we just get the idea from them.
* If we have time, we also want to add convolution layers or do more, and we also want to train letters. But it will be more compicate for letter and number mix together. 