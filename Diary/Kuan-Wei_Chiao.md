Written by K.W. Chiao

# 2020-03-21 - 2020-03-22#

Video Watching <[How Far is Too Far? | The Age of A.I.](https://www.youtube.com/watch?v=UwsrzCVZAb8&t=607s)>

Episode | the first episode of The Age of A.I. / Publish Time | 2019-12-18

### Topic | Building A.I. Avatar for William

Mark Sagar | CEO, Soul Machines

- Mark Sagar said, "The best way of improving the cooperation of human and machine is to make A.I. as life-like as possible."


- [Baby X](https://www.auckland.ac.nz/en/abi/our-research/research-groups-themes/laboratory-animate-technologies.html)
	
	- **Baby X**
is a interactive svatar based on neural network models of the human brain.
Unlike many virtual assistants, it provides **life-like** interaction.

	- **Object recongnition**
	
	- **Stress system** if give her a fright, it will simulate releasing noradrenaline.

DR. Ayanna Howard | Roboticist

- One of the biggest misconceptions of A.I. is that it kowns all, can do all, smarter than all of us put together.

- A.I. is built on us.

- Older adults are happier with a robot that emotes and is social than having no one there.

### Topic | Musicual prosthesis

Gil Weinberg | Founder director of [Georgia Center for Music Technology](https://gtcmt.gatech.edu/)

- People are concerned about A.I. replacing humans, but I think it's going to enhance humans.

- [Shimon](https://www.shimonrobot.com)

	- **Shimon**
	is designed to create meaningful and inspiring musical interactions with humans.
	
	- **Real-time collaboration** | Allow human and artificial players to cooperate and build off each other's ideas. 

	- **Visual Motion** | Connection between sound and motion allow humans to anticipate, coordinate, and synchronize their gestures with the robot.
	
	- **Machine Learning** | It will try to see what note is he likely to play after what note. 
		And once it finds its **patterns** it can start to manipulate it and play with combination of different style. 
		
- [Robotic Drumming Prosthesis](https://www.youtube.com/watch?list=PLA5dALc3G8POn27MIzSNyVx2WFWm8iRWo&v=ntrlHw6f4E4)

	- The prosthesis attached to amputee is designed with two sticks.
	
		- First | Controlled by mucisians's arms physically and EMG muscle sensors electronically
		
		- Second | Listen to the music being played and improvise.
		
	- In some ways, the robotic drum arm allows amputees to play better than they ever has, 
		but it still **lack the true funciton or feeling**.
		
	- They don't provide the kind of dexterity and subtle control that really allow everything.

- [Skywalker](https://gtcmt.gatech.edu/robotic-musicianship-projects#skywalker)

	- Currently, most advanced prosthetic hands can only open or grip using five finger at once.

	- **ElectroMyoGraphy EMG** | Most used by prosthetics on the market nowadays. 
		
		- Two Sensors | Contact with residual limb, Pick up elctrical signals form muscles.
		
		- Problem | The electrical signal is vague and not accurate.

	- The prosthesis is designed for **finger-by-finger operation** with dexterity and subtlety.
	
	- Ultrasound sensor and deep learning algorithms | **Detect & Predict** muscle patterns in the amputees' stump.
	
		- **Ultrasound sensor** | Provide image inside the arm to visulize amputee's intention.
		
		- Machine Learning | Predict the patterns letting the man lost one of his arm to move all five fingers individually.

DR. Ayanna Howard | Roboticist	

- Gil comes from a non engineering background, which means the way he thinks about robotics 
	is quite different than coming from engineering background, like me.
	
- The commonality is that we want to design robots to really impact and make a difference in the world.

Notes

- **Neural Network** | is a simple virsion of human brain.

- Today, most avatars can answer simple questions and give scripted responses.
So the next target is to create avatars that can actually learn, interpret, and interact with the world aroud them, like a real human.

- **Object recongnition** | Tell the difference between things, which people do naturally, A.I. need to learn from the scratch by search the **pattern**.

- **Affect Computing** | Interpret and simulate human emotion, which gives machines empthy.

- **Machine Learning** | The ability to find patterns in data.

# 2020-03-28 #

Read < [Elements of AI - Introduction to AI - Chapter 1: What is AI?](https://course.elementsofai.com/1/1) >

## I. How should we define AI? ##

Application 1. Self-driving cars

Application 2. Content recommendation

- algorithms involve filter bubbles, echo-chambers, troll factories, fake news, and new forms of propaganda.

Application 3. Image and video processing

- This challenges the notion that “seeing is believing”.

What is, and what isn't AI? Not an easy question!

- Reason 1: no officially agreed definition
	- certain methods for processing uncertain information are becoming so well understood that they are 
	likely to be moved from AI to statistics or probability very soon.
	
- Reason 2: the legacy of science fiction

- Reason 3: what seems easy is actually hard…

- …and what seems hard is actually easy

So what would be a more useful definition?

- Autonomy | 
	The ability to perform tasks in complex environments without constant guidance by a user.

- Adaptivity | 
	The ability to improve performance by learning from experience.
	
Words can be misleading

Why you can say "a pinch of AI" but not "an AI"

- Thus it would sometimes be more appropriate to talk about the "AIness" 

- If you'd like to talk like a pro, avoid saying "an AI", and instead say "an AI method".

## II. Related fields ##

- Machine learning | 
	Subfield of computer science; Systems that improve their performance in a given task with more and more experience or data.

- Deep learning | 
	Subfield of machine learning; Nncrease this complexity not only quantitatively but also qualitatively different from before. 

- Data science | 
	Umbrella term; 
	
- Robotics |  
	Combination of virtually all areas of AI.

## III. Philosophy of AI ##

- The Turing test | 
	If the interrogator cannot determine which player, A or B, is a computer and which is a human, the computer is said to pass the test. 
	
- One problem: does being human-like mean you are intelligent?

- Is Eugene a computer or a person?

- The Chinese room argument

- Is a self-driving car intelligent?

- How much does philosophy matter in practice?

- General vs narrow AI | handles one task or any intellectual task(AGI). 
	
- Strong vs weak AI

Video Watching <[Healed through A.I. | The Age of A.I.](https://www.youtube.com/watch?v=V5aZjsWM2wo&t=1342s)>

Episode | the second episode of The Age of A.I. / Publish Time | 2019-12-19

### Topic | Enablind peole with neurological disease to communicate





### Topic | Combating one of the leading causes of blindness

[Tim Shaw](https://www.youtube.com/watch?v=BN8-r46Dt5Y) | Former NFL player

- I think the hardest part is between that I can still talk but not everything I say is understandable.

- **[Amyotrophic lateral sclerosis (ALS)](https://en.wikipedia.org/wiki/Amyotrophic_lateral_sclerosis)** is a disease that causes the death of neurons controlling voluntary muscles
	
- I feel smarter than ever, but I just can't get it out.

- As, I am losing a lot of my life, one thing I stand by is that If it's worth doing, I'll do it.

- I view this disease as a opportinuity, if I can impact someone else then that is enough.

Julie Cattiau | Product Manager, Google AI

- [Project Euphonia](https://ai.googleblog.com/2019/08/project-euphonias-personalized-speech.html) |  Convert a person’s speech into text
	- Current state-of-the-art Atomatic Speech Recognition (ASR) models can yield high Word Error Rates (WER) for speakers with only a moderate speech impairment from ALS.
	- **Two-Phased Approach to Training** | From fine-trained ASR model to personalized model
		- Typical speech recognizers are trained with large corpus, but it is nearly impossible to acquire that much data fronm a single speaker.
		- The personalized model trained with the targeted non-standard speech characteristics only need a much smaller dataset.
	- **The Neural Network Architecture**
		- RNN-Transducer (RNN-T) | Two layers
		- Listen, Attend, and Spell (LAS) | Three layers
	
	
- [Parrotron](https://ai.googleblog.com/2019/08/project-euphonias-personalized-speech.html) | Convert a person’s speech directly into synthesized speech

Frenando Vieira | Chief scientific officer, ALS TDI

- When the ice bucket challenge happened, we had this huge influx of resources of cash, and that gave us the ability to reach out to people with ALS who are in our programs to share data with us.

- That's what got us the big enough data sets to really attract Google.


Michael Brenner | Harvard professor & Research scientist, Google



Notes
- What if we could improve diagnosis? Innovate to predict illness **instead of just react to it**?
- Can we build one machine that recongnizes many different people, and how can we do it fast?

[14:43](https://youtu.be/V5aZjsWM2wo?t=883)

# 2020-04-16 #
Material 3 | I learned a little before so this material is easy for me.

# 2020-04-18 #
Material 4 | I hadn't used <class> in python or tensorflow, so this in interesting to me.

# 2020-04-23 #
Last week I didn't konw there was a online class. Today I got in however without check......

Material 5 | 

The video is very useful. (Network is function of neural function | weight function)

# 2020-05-03 #
Material 5 | The amount of information in textbook is too much, hahaha.
5.1 Task, T / Performance Measure, P / Experience, E 
5.2 Capacity, Overﬁtting and Underﬁtting / The No Free Lunch Theorem / Regularization
5.3 Hyperparameters and Validation Sets / Cross-Validation

# 2020-05-09 #
Material 6 | The video is quite clear but I still need some time to fiqure it out.

# 2020-05-14 #
The lesson yesterday was qutie interesting bacause of the discussion of AI with classmates.

# 2020-05-24 #
Material 7 | more focus on theory,so I think i need more time to get used toit.