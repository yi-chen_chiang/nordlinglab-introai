This diary file is written by Marc Henneberger N16088440 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* While looking through success stores written by other students, I realized that I didn't know that there are so many medical applications for AI. I thought the topic was more about general image recognition, self driving cars etc.
* What are the distinct terms and differences between AI, Machine Learning, Deep Learning, Neural Networks etc.?
* How do you assess the quality of an AI? Does it make more sense to test it against randomness or against the best human in the specific field? 
* Which programming languages are most commonly used for Artificial Intelligence? In the source-article for my success story, the AI was programmed using python and tensor flow. Are those widely used in industrial firms as well as in science labs? Anyways, it seems good that we will be learning those languages while learning about AI. 
* How to proceed after taking this course if we want to go further into AI professionally? Is a Computer Science degree needed to work with AI?

# 2020-03-29 #

* Calling AI Methods not just "an AI" really makes more sense. It makes it much clearer to see an AI Method not being some "magical" programme itself but rather a bunch of methods within Computer Science to apply within other programmes to serve a common goal. 
* I would doubt that Robotics are the key discipline of AI. There are many robots working without AI Methods. Even considered that AI Methods would enhance the robot's behaviour in some ways, AI seems more of a more generic discipline.
* How far can one programme improve itself through machine learning? What limits are reachable? Does more and more learning data necessarily result in, for example, nearly 100% correctness of image recognition? And if not, where are the limits and how to reach them?
* The proposed definition of an AI Method "Autonomy and Adaptivity" makes sense. I have not yet thought of being able to describe AI using only these two words but it seems that these are describing the core of AI.

# 2020-04-05 #

* This week I looked into the basics of the Python programming language. 

* First I had a talk with a friend of mine who is studying computer science. I learned that Python is the most useful language to use when it comes to AI topics. With many libraries available, it is possible to concentrate on the AI Methods. 

* To get to know the basics of the language, I watched a YouTube Video. The syntax of the basic functions (variable types, conditionals, basic loops) are explained. I'm planning on watching the video further next week. 

* At the first sight, it seems to be a language which is quite easy to use since the syntax seems to be minimal and self-explanatory. For I don't have much experience with programming, I hope that I will be able to learn quickly enough to work with AI methods effectively in this course. 

# 2020-04-11 #

* This week I learned further things in Python. For training purposes, I made a programme, which identifies the longest word in a list of words and gives out the word and the amount of characters this word has.

* I just recently found the Nordling Lab Python Tutorial. I haven't found it earlier because it was only in the section "Lecture material 2019" in the Readme.me at Bitbucket, but not in the 2020 Section. I also used it for getting more familiar with Python. Since its explanations are quite short, I decided to go with the Nordling Lab Tutorial for an Overview and with the Youtube Video if further explanations are needed on a specific topic.

* I realized that we are currently in week 6 of the plan in "Materials and Assignments 2020" in the Readme.me on Bitbucket. Because of the confusion at the beginning of the course, I assumed we were currently in week 4. Hopefully it is not too stressful to catch up again. At a first glance on the topics of week 5 and 6, it seems doable though. 

# 2020-04-19 #

* This week I learned more things about Python. I wrote a sorting algorithm (Bubblesort) to get more familiar with the loops in Python. I also refreshed some old knowledge about object-oriented programming from a project regarding UML. (How do classes, objects, attributes, methods work? What is their purpose etc.) 

* I also attended the online lecture on tuesday which is why I now have a better understanding about the organisation of the course. Also the requirements for the group project became much clearer. 

* I watched the Youtube-Videos about neural networks. (The one in the readme and the following one on the channel) Both were very well able to explain the basics of neural networks. I also started to read the chapters in the book. Because the book is written in a much more theoretical and complicated way, I underestimated the time, I would need to read and fully understand the content. So I did not finish the chapters by now. 

* By reading and watching all those sources, I now have a much better abstract understanding about what AI contains. Now, I am very interested to see how an AI Method is actually built up in terms of writing the code. 

# 2020-04-26 #

* This week, I finished reading the chapters in the book. While the Youtube Video gives an overview about neural networks and the "switches" to play with to change the network's performance, the book focuses more on a theoretical and more basic approach to machine learning building up from linear regression. By explaining those two concepts side by side, I feel that a top-down and bottom-up approach of teaching is applied simultaneously which seems to be a good approach.

* I watched the beginning of the Introduction to Tensorflow.

* Also, I attended the online lecture on thursday afternoon. The additional information added to the readme helps to keep a better overview about the structure of the course. I also highly appreciate the recommended dates for the tasks as it helps to see where I'm actually being right now in terms of progress.

* That being said, we are currently in week 8. Because of other classes' homework, I wasn't able to invest as much time in AI as I wanted but I feel like it is still perfectly managable to keep up with the tasks. This also matches what the professor said during the first online lecture.

# 2020-05-03 #

* This week, I unfortunately was not able to spend as much time to AI as I would usually do because of other subjects' midterm exams.

* However, I looked further into the course of Martin G�rner. It seems to be organized really well. The Colaboratory Code is working well, even though I had to adjust it a bit because of an error message coming up. Someone told me to change "tf.encode_raw" to "tf.io.encode_raw". It worked using this small adjustment.

* I am wondering, how much of a difference in terms of computing power lies in the GPU/TPU acceleration. I briefly looked into how to install tensorflow on my local PC and it seems that some sort of acceleration is highly recommended/necessary to successsfully run it. Not having a really powerful PC, I don't know how quickly the limits are reached.

# 2020-05-07 #

* This week, I catched up a little bit to the recommended plan in the readme file. I watched the Tensorflow Video, analyzed the code and went further on to other tasks.

* I read the excerpt from the AI book about convolution and pooling and watched the video about the same topic. I have yet to understand how exactly neural networks can benefit from convolution- and pooling layers the most, i.e. how many layers should be applied and in which order. But I guess, I will develop a better feeling for these things with having practical experience in dealing with neural networks.   

* Because I watched the video about gradient descent some weeks ago, I have already accomplished a task which is scheduled for later. Having done some frontloading feels good.

* We are now in the week in which the group project work should start. I decided to do the group project alone for I am confident in accomplishing the task on my own. Moreover, I assume that organizing meetings with people having different schedules etc. would be more stressful in the end than the contributions are helpful. Some friends of mine do the project alone as well, some friends do it in a group. I see the advantages for both sides and can also understand people making a different decision. In the end, we will see how it turns out for all of us. But I believe that the given task is on the one hand doable without asking too much of us and on the other hand encouraging and motivating to learn a lot about AI, whether we accomplish it alone or in a group. 

# 2020-05-17 #

* This week, we had our first in-class lecture on thursday afternoon. We discussed different aspects of how AI can get into conflicts with ethical questions. It was interesting to discuss those topics in class with many students sharing personal experiences. However, it was a bit unfortunate that we did not have much time to get into deeper discussions. We discussed many topics but did not further elaborate on most of them. Nonetheless, it was a good and interesting lecture.

* The videos that we should watch before the lecture were very good in terms of raising awareness about various aspects of ethical questions regarding AI. I especially liked the "stop button problem" which I have not thought of prior to watching the video.

* I also looked into how to work with Google Colab and Jupyter Notebook. It seems to be quite a convenient way to code and also submit the code once it is finished.

* Furthermore, I read all the materials about the backpropagation algorithm. Now I feel like having even more insight in how a neural network actually learns. It is always impressive to see how algorithms only are able to accomplish tasks like classifying numbers and mimicking human intelligence by doing that.

# 2020-05-25 #

* This week, I worked on my article summary. I found an interesting article which enhances a neural network's performance by using a training strategy which was inspired by biological evolution. 

* I also did some basic research about how to approach the group project. Because of other important tasks to do, I did not start actually writing the code but I'm confident that because of all the reading and looking at examples beforehand, I have a strong knowledge foundation which will help me to complete the project successfully.

# 2020-05-30 #

* This week, I began working on the project work. I started by trying to combine the information presented in Prof. Nordling's Google Colab Tensorflow Tutorial with the information found in Martin Gorner's Colab. Unfortunately, it seems that both are using slightly different approaches in terms of structuring the MNIST Data which is why I need to figure out how I can apply techniques from Gorner's tutorial without having to copy all his code and therefore maintaining the simplicity and understandability of Nordling's approach.

* Besides that, we had in-class teaching again last Thursday. We talked about how future technologies will affect matter of human work very holistically. We covered definitions of industrial revolutions, self-driving cars, electric vs. combustion engines, power generation and deployment and many other topics. Unfortunately, we weren't able to finish the lesson which should also include what will be important for us and our dream-jobs which we talked about in the beginning of the lesson. However, we will be covering those topics in the subsequent lesson next week. 

* I also finished the powerpoint-presentation of my article summary. It took a bit more time than expected because I created some graphical features (diagrams, pictograms to illustrate information) myself in order to make the topic easier understandable for listeners. But I'm quite satisfied with the outcome. I also adjusted my success-story presentation to fit it to the latest template and polished it up to make it a bit more tidy and organized.  

# 2020-06-07 #

* This week, I made some big steps forward in building up my neural network for the project work. I looked more deeply into how the keras-syntax really works and how to deal with the MNIST-Data. Right now I have a code which is able to build a neural network, train it, validate it and plot the results nicely. The structure is finished, so to speak. The next task will be to experiment with various kinds of layers, convolution, pooling and other tricks in order to improve the accuracy. But having the code all set up for that, I'm looking forward to continuing with my task.

* I also attended the class last Thursday. We finished last week's lecture and talked further about which professions will be affected in which way by automation. In the end, we had a little discussion about how we would predict the influence of automation in our dream-jobs and hearing all the various aspects of automation was very interesting.

# 2020-06-15 #

* This week, I mostly took care of the project work. I experimented with different kinds of layers and discovered that the time to train the network highly depends on the number and types of layers used. No wonder that convolutional neural networks became popular by the time, the computing power has become high enough. I'm still trying to improve the overall accuracy but I have managed to get quite some decent results which I will be presenting upcoming thursday.

* Last Thursday, we also presented our article summaries or at least some of us. Even though, I volunteered to hold my presentation next wednesday, I was happy that there was still enough time for me to present this time. I am happy with how the presentation turned out to be. I got some good feedback from friends that I was able to convey the information in a way that was easily understandable. This has made me really happy since I think that presenting a topic to an audience is a valuable skill which is important to learn.

# Article summary #

* While Backpropagation as the go-to method to train a neural network usually works quite well, it, however, faces some problems occasionally. One of the most famous problems is becoming stuck in local minima resulting in suboptimal performance while a state with an even smaller loss function may be within reach as well.

* In this article "A Hybrid Method for Training Convolutional Neural Networks", Vasco Lopes and Paulo Fazendeiro propose a hybrid method where conventional training using only backpropagation is combined with evolutionary strategies inspired by genetical evolution found in nature.

* The procedure works by using only backpropagation at first to get to a level of a solid performance. Once this level is reached, backpropagation continues but with evolutionary procedures coming in place every few epochs. Their goal is to split the weights and biases of the output layer into a broader range of parameter-sets and evolve them in specific ways in order to obtain more parameter-sets whose loss function might be even smaller than the original one found by backpropagation. 

* To do that, the weights and biases of the last layer found by backpropagation become randomly shifted smoothly to obtain a "population". New individual parameter-sets ("individuals of the next generation") are then built up by either combining certain parameters from individuals of the old generation ("crossover") or by randomly shifting parameters in total or in parts ("mutation"). This goes on for 5 generations. Then, the best parameter-set is used to continue backpropagation from.

* Using this method, the researches were able to improve the accuracy of classifying colored images (CIFAR-10) by 0.61% on average. The time cost, however, was about 4 times as high. Both numbers were obtained comparing a convolutional neural network (VGG16) trained using only backpropagation to using the hybrid method; both for 100 epochs.