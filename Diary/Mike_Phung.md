This diary is written by Mike Phung for the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

It contains the summary of todays lecture on Introduction to Python.

# 2021-03-05 #
  * Review Lecture 2 (2021-03-04)
  * Read chapter 2-5 in the book: Automate the Boring Stuff with Python
  * Read an intersting article, namely, "Demystifying deep learning" (https://bdtechtalks.com/2021/01/28/deep-learning-explainer/?fbclid=IwAR2nASkCv2HXcIzUz8SliWaVh_9FXsl1-rRvR0W0E_31YMhEW4YG6DkdjR8)
  