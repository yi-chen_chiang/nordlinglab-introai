This diary file is written by Rio Ananta Perangin-Angin E64057089 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20

* The first lecture was fine. I'm not actually really into AI but the success story seems like very promising.
* I start to think to continue to take this class because there's might be something that I can apply in my field.
* I was nervous when I found out that we will do some programming because my laptop basically incapable to run such things

# 2019-02-27

* Today I learned about the difference between AI, Deep Learning, Machine Learning, Data Science, Data Mining.
* I don't actually like discussing about their definition. Eventhough their definition can help us to differentiate each one of them, I don't think its neceassry for understanding AI. For an introductory course, I think it is way too much. I think we just can start with AI only.
* We learn about the definition and Mr. Nordling mention his favourites, but I haven't choose my favorite definition yet because it requires some reserach and deeper understanding.
* Now I understand how statistics plays major role in data management.
* The coolest part of AI for me is the ability to do some task without being explicitly programmed to do so. We can utilize this skill to substitue human labor needed for data anylisis.
* I really wonder if we can program an AI without being explicitly programmed to program another AI. i.e. We use AI to make more AI.

# 2019-03-06

* There are some similarities between Python and Visual Basic that I haave learned before.
* I already understand how to use loops and conditional statement in Python.
* The professor didn't mention about whether Python case sensitive, but later I figure it out while doing some exercise provided.

# 2019-03-13

* I was a little bit skeptical about this class in the beginning, but actually it's getting better than what I've expected.
* I found that the usage of Google Collabs is very effective. I have attended some lecture about programming before, and in my opinion, this is the fastest and the most efficient way of studying programming.
* Mr. Nordling promised us that the programming class going to be very basic. What I really like about that is eventhough I have attended some programming class before, it's not necesarilly makes this class become easier. I still found this class to be challenging and exciting.
* I really appreciate the presence of teacher assistans. They are always helpful and they always available for answering all my questions regarting Python makes this class even become more effective than what I can expect. The facts that they are not paid really teach me something about sincerity. Even though this class is not purposely designed to teach is stuff like that, but you can alwasy learn something new from people.

# 2019-03-20

* The video from 3Blue1Brown really help me to understand better about thte Neural Network.
* I found that Neural Network is extremely complicated. Even though I understand the basic concept about the neural network, I think it going to be very difficult to build some neural network to solve actual problem.
* I know that programming, especially when we plan to do something with a lot of data, will be related a lot with mathematics. But understanding how we can make such a complicated thing become very simple by using matrix is very interesting to me.
* I don't think Supervised Learning, Unsupervised Learning, and Reinforcement Learning is interchangable. What I meant by this is if a particular problem can be solved by machine learing that use supervised learning, then it's not possible for us to solve the problem by using machine learning that use unsupervised learning. For example, we cannot provid label/answer to AlphaGo (or Game AI) since we ourselves don't actally know what the answer is. Therefore, it's imposible to design the AlphaGo by using machine learning with supervised learning.

# 2019-03-27

* Backpropagation is a very smart algorithm to asist the learning process of neural network. I would argue that this method is the key of making accurate neural network system to learn as fast as possible.
* Using backpropagation algorithm does not necessarily shrink the amount of data sample required for machine to learn. We still need to provide a lot of sample to be used for the system to learn.
* I really wonder if there's such an algorithm that we can use to help us to build some neural network but with much less data sample required.
* Maybe all models are wrong because we still neglect most of the uncertainities. Meanwhile, if we include everything on our model, it is not worth it since its consume way too much resource without adding much benefits. 
* I found the idea that we can do some manipulation when performing backpropagation is very smart. For instance, we increse the weight of neuron with higher activation value is more important than increasing the weight of neuron with lower activation value. This means, the process of increasing weight can be optimised. We really need to understand this optimization process because otherwise we might end up doing something useless when using backpropagation algorithm.

# 2019-04-10
* Today class is a little bit less interesting than the previous class. The reason for me is because the topics about Convolutional Neural Network seems like irrelevant with my future work as a civil engineer.
* I dont find the topic we learn today is 'doable' for begginers like us. It's already very advanced topic without real applicable work that we can do.
* In the class, Prof. Nordling said that if I am trained to be an engineer, than I really need to understand about the filtering process. After Prof. Nordling explained about this, I then realize that these topics is actually relevant to my field of study.

# 2019-04-17
* After listening to today's lecture, Prof. Norlding started to explain more detail about how we can build our AI by ourselves. After listening to the explanation, I then know that making and train our own AI is possible.
* However, after listening to the lecture, I then understand that most of the work is actually done by other people. For example, our trainig data set is already done by MNIST.
* I am a little dissapointed because the fact that most of the hardwork is done by other people. I feel like we are about to build our AI only for the sake of 'build and train an AI". We harldly do any work at all.
* I then realize if we want to do everything by ourself, then it will take a lot of effort and impossible to be taught in 1 credit general education course.

# 2019-04-24
* I successfully trained my AI at home.
* Im facsinated by the fact that we can analyze the accuracy of our AI and mine is 94.14%
* As I expected, we hardly do any substantial work at all. Most of the process we only use what poeple have done and share. Hence, further study is really important if someone want to understand in more detail.
* However, about building and training AI, I at least know 'what to do' eventhough I don't really know 'how to actually do it". And I guess this is good enough considering we only spend about 1~2 hours per week on learning this.

# 2019-05-01
* My question is: when the professor told us that building an AI is more like an art than a science, is that also means that there's to right or wrong method to do it? 

# 2019-05-08
* The lecture today basically only review more about CNN and backpropagation

# 2019-05-15
* The issue about AI replacing human has become an most discussed issue recently, not only in our class. Tragically, this will impact our economy because AI can replace human in almost every industry.
* Beside talking a lot about the AI, we as human also need to start thinking about our own future. We need to have something special that cannot be just replaced by AI
* I guess, AI may bring chaos to our civilization, not because AI will start a war, but because AI will cause a lot of instability in our economy system and will cause some crysis.
* I found the idea of controlling and supervising the development of AI is very imporant

# 2019-05-22
* Bias that occurs during analysis is very reasonable. I think the most important thing is how we keep improving the system so that biases can be minimalized. But of course it will be different case if someone cause the bias intentionally.
* The idea of accountability of AI development and its impact in our society is very imporant and I think every people must concern about this too, not only those who work to build the AI.
* I think international community/organization must start making the regulation about the development of AI, strict enough so it wont cause an instability but not so strict so that it slows down the development of AI (the point of using AI is because it develops in rapid progress)

# 2019-05-22
* Meeting with teammates for the group project
* We start to modify our model, but still getting the result worse than the model provided in the class
* The best we can get so far is 94%

# 2019-06-05
* Continuing the project
* Until today's class end, the result still worse than the model provided in class
* We need to do the project in other time because we also want to presenting our discussion
* Later on this week, we successfuly make a model that is better than the sample. We actually got 100% on the accuracy which is mindblowing
