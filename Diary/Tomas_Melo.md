This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Tomas Melo Peralta F04057176 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* I learned about AI in board games
* The same technology may be used in the future for diagnostics
* I'm curious about how group work will be handled in this online class
 
# 2020-03-29 #

* I hope we can get into Python soon
* Seems like there is no assignment for this week???


# 2020-04-05 #

* I'm concerned o I don't know how to view each week's assigments, since it's been two weeks without homework, apparently
* Watched the video on neural networks instead

# 2020-04-12 #

* I took a look at the python tutorial 
* I was more interested in google colaboratory than the actual python tutorial
* It verypractical since no software download is required to start practicing

# 2020-04-19 #

* I read the new powerpoint slides provided
* I continued to practice python
* I refreshed my memory on the following methods: range, for, while

# 2020-04-26 #

* I did the reading on Machine learning by Ian Goodfellow and Yoshua Bengio and Aaron Courville
* The way "learning" is characterized in terms of P performance, E experience, and T tasks is very intuitive
* I llearned there are several types of tasks such as: classification, regression, and transcription
* On a separate note, this week I used a github to get some data for another class. It was verysimilar to bitbucketso I could manage through the content with relative ease.

# 2020-05-03 #

* This week I continued 3Bule1Brown series on neural networks
* I got a very rough idea on the concepts of gradient descent an backwards propagation, but I will have to review the videos since I still have several doubts
* I think building a neural network will prove very challenging


# 2020-05-17 #

* I read Chapter 1: Using Neural Networks to recognize Handwritten Digits
* Towards the end it focuses more on the code used for the neural network
* So far, the coding is my biggest difficulty
* I copied the authors provided code, but I still have some diffculties understanding what it does

# 2020-05-23 #
* Last week's class was very interesting
* We talked about some of the dangers of AI, especially the viability of a general purporse AI
* I concluded that maybe this type of AI should not be developed yet

# 2020-05-30 #
* I have attempted to build my neural network.
* Despite the videos and code examples, I'm having a hard time understading the order of tasks I have to take in order to achieve a satisfactory neural network
* I created and started my first Jypiter notebook
* I am still confused about the relation between Jupiter notebook and Google's Colab
* I believe automatization is an inevitable step forward
* The advance of technology will open way to automatization in scales never seen before
* As for my fream job, I want to own my own company to give off grid power alternatives 

# 2020-06-07 #
* I have been working on my jupyter notebook
* Soon I will be ready to upload the video
* I did some googleing into Keras and its applications

# 2020-06-07 
* I worked on my code for the neural network as well as the Jupyter notebook that goes with it
* I really like Jupyter notebooks; as a didactical tool it has great potential
* I had to take a more in depth look at the MarkDown syntax for the elaboration of my Jupiter notebook
* I had trouble to upload the files using git bash
* I did manage to download gitbash follow the instructed commands, but an error occured when trying to upload my files
