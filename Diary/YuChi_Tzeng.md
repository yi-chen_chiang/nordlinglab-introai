This diary file is written by YuChi_Tzeng C14041117 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* After being confused for a week of not finding "the video of the week", I finally figure out what assignments I should do in Sunday morning.(what a great start...QwQ) 
* After reading "What is AI", I learn that there are different kinds of AIness, and have a clearer view of the definition of AI.

# 2020-03-25 #

* Wait...what are we doing this week?0_o
* (Brain processing the info page...)
* (Brain reprocessing the info page...)
* !error!
* Ok, must be me being blinded again...I will come back for this later...Got too much of other assignments to do & too less of time for every single one of them.@~@
* To be...or not to be...

# 2020-04-01 #

* It looks like this week is like last week : Study Python.
* Even though the 6 days national holiday and spring break starts from this week, I'm as busy as I'm during the normal weeks, maybe even more busier, because I want to push the progress of a group project from another class as hard as I can.

# 2020-04-08 #

* So...after the holidays, no one can go anywhere without a mask...= =
* The "But what is a Neural Network? | Chapter 1" is kind of interesting. It felt weird at the beginning, cause 3blue1brown skips a lot of details while bringing up a lot of concepts, but after watching it a few times, I have a clearer view of Neural Network. Some analogies in video make understanding the concepts a lot more easier, such as, Neuron is like a fuction.

# 2020-04-15 #

* Midterm weeks! Ya, I'm still busy...@~@
* So, due to the lack of time, I only watch the video from this week's assignments & browse through other link pages quickly.
* Oh, wait a sec...(o_O) This is our group project topic!0口0
* Well...good thing that I realize that now... I guess I know what I'm going to be buzzing on after the midterms...@~@

# 2020-04-22 #

* meow~
* I finished the ppt for the AI success story this week, I picked MedCheX for my topic, because it is kind of a big news in Taiwan(specifically for NCKU) recently, and for some reason no one has snipped it.
* So from the info I read, the members of the MedCheX team has been doing some projects that is similar to MedCheX before COVID-19 breaks out, which seems to be the main reason they won the COVID-19 Global Hackathon in my opinion.
* MedCheX is still a WIP project, because we can see some projected future plannings on their DEVPOST page, and also as we learnt from last week's video, 92% accuracy is horrible.
* Overall, MedCheX saves a lot of time for doctors, which appears to be simple, but sometimes the simplest thing tends to make the biggest change in real life.

# 2020-04-29 #

* (=^-ω-^=) 

# 2020-05-06 #

* I wonder how long will it take for 5 people to get in the line group, after I sent the line invitation to their gmail & moodle...

# 2020-05-13 #

* I tried to push a few group projects to be done before June, so that I can send many time on the group project of this class, because this one is out of my confort zone...but things usually don't go the way we want...= =
* I'm definitly not abusing the attendance system in the past buzy weeks...(;´･ω･)
* The first three videos discuss about what we create now will affect the future, and the AI facual reconization bias problem show that it has already started affecting our community.
* And the other two videos, first one is a bunch of weird assumptions, it makes a little bit of sense after I watched the second one.
* I feel like the last two videos are showing us the possibility of how our current AI technology level reachs to the point that the robots, we created, have enough intelligence that they know what is good for human better than human does and will disobey human's order if they don't think the human is correct, just like in some sci-fi movies.

# 2020-05-20 #

* This week I had the first physical in-class lecture of this course and I guess the asking questions part is to inspire us to think about the ethics of AI, which I think it is important, if we are going to develop something involve AI in the future.
* The last two question are kinda confusing, and I couldn't figure out the meaning of them...
*  How do we stop AI doing what we don't want them to do without a stop button?
*  How do we treat each other fairly?
* Although I was thinking about them for a few times after the class, and kinda figure out how to understand them.
* If we swap the objects of them(like below), and look at the 4 question at the same time, then the meaning of the questions is more clearer to me.
*  How do we stop each other doing what we don't want them to do without a stop button?
*  How do we treat AI fairly?
* I guess the meaning of the questions is "In the future, if we developed AI to a certain intellegent level(maybe as same as average human, or higher...) that AI have feelings, such as confidence that is mentioned in the last video, then we can't design AI with the way we are doing now, treating AI as a thing that only obey orders without thinking, instead we need to treat AI as a new kind of human, so that we can keep on improving our life with the assistance of AI and without hurting AIs' feeling.
* (In other words, it will come to a time that human no longer control AI, but cooperate with AI, and the time may not be too far from us in my opinion.)

# 2020-05-27 #

* This week, Pin-Feng  Yu, Benenedictus Kent Chandra and me went to the main library to discuss about the group project, because Romaine Parker had a huge midterm this week so he couldn't meet up with us, but he said he would help us by observing his friend's project later.
* We were discussing about the main part of the project is to improve the accuracy of the program professor gave us, and trying to figure out how to see the result of professor's code.(he said he made it's accuracy bad intentionally)
* The steps we figured out during the meeting:
* softmax
* mnist(result)
* tensorflow

* relu
* learning rate delay
* overfitting - dropout
* convolutional layer
* convolutional neural network
* bigger convolutional network+dropout
* We guess that the starting step may be relu, which made the steps looks really short(we organized it from the 55 minutes video in the bitbucket.), and that's why I'm checking the bitbucket for more detail.
  Because the steps seem weird for us to split it, so instead we decided to work on the steps together after we figured out professor's code and the accuracy improvement steps.
* We left with some objective :
* Pin-Feng  Yu and Benenedictus Kent Chandra figuring out how to move professor's code to the "Martin Gorner's Colab"(in bitbucket6.1.), which is where we will be testing our code;
* and me figuring out more details of the steps of improving the code's accuracy.
* I imformed Romaine Parker about what we discussed about after the meeting, and asked him to start with listing the accuracy improvement step of his friends' project, so we can see the differences & selections his friends did in their project compare to the resources professor gave us(I'm listing it).
  Then Romaine Parker and me can start helping Pin-Feng  Yu and Benenedictus Kent Chandra with the code.

# 2020-06-03 #

* My dream job is to be a engineer that is capable of different field, so that I don't get stuck in the project I am working on due to the lack of specific field.
* I would like to have freelance work of work security.

# 2020-06-10 #

* Group project...(=^-ω-^=)
* 

# 2020-06-17 #

* I feel that watching peoples' projects for a whole physical class is a waste of time, because that and even the Q&As are things that can be done on the bitbucket...
* Group project：https://www.youtube.com/watch?v=d4mvKBoAoNI
* Finally done...@_@

# 2020-06-24 #

* 
* 

