import matplotlib.pyplot as plt

import numpy as np
np.random.seed(10)

from keras.datasets import mnist
(train_feature, train_label), (test_feature, test_label) = mnist.load_data()

# feature data preprocess
train_feature_vector = train_feature.reshape(len(train_feature), 784).astype('float32') #reshape(28*28 to 784*1)
test_feature_vector = test_feature.reshape(len(test_feature), 784).astype('float32') #reshape(28*28 to 784*1)
train_feature_normalize = train_feature_vector/255 #normalize(0-255 to 0-1)
test_feature_normalize = test_feature_vector/255   #normalize(0-255 to 0-1)

# label data preprocess
from keras.utils import np_utils
train_label_onehot = np_utils.to_categorical(train_label) #onehot encoding
test_label_onehot = np_utils.to_categorical(test_label)

# MLP model
# Sequential model
from keras.models import Sequential
model = Sequential()
# input layer & hidden layers
from keras.layers import Dense
model.add(Dense(units=256, input_dim=784, kernel_initializer='normal', activation='relu')) #input layer: 784, hidden layer: 256
# output layer
model.add(Dense(units=10, kernel_initializer='normal', activation='softmax')) #output layer: 10

# training
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x=train_feature_normalize, y=train_label_onehot, validation_split=0.2, epochs=10, batch_size=5, verbose=2)

# evaluate the accuracy
scores = model.evaluate(test_feature_normalize, test_label_onehot)
print("scores: ", scores)

# prediction
prediction = model.predict_classes(test_feature_normalize)

# show image 
def show_images_labels_predictions(images, labels, predictions, start_id, num=20):
  plt.gcf().set_size_inches(12,14)
  if num>25: num=25
  for i in range(0, num):
    ax=plt.subplot(5,5,i+1)
    ax.imshow(images[start_id], cmap='binary')
    if(len(predictions)>0):
      title='prediction= '+str(predictions[start_id])
      title+=('(O)' if predictions[start_id]==labels[start_id] else '(X)')
    else:
      title = 'label = '+str(labels[start_id])
    ax.set_title(title, fontsize=12)
    ax.set_xticks([]);
    ax.set_yticks([]);
    start_id+=1
  plt.show()

show_images_labels_predictions(test_feature, test_label, prediction , 0, 20)
